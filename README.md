# Mesowest Temperature

Alexa app to check the Mesowest API for the current temperature

## Building

Install dependencies to local dir:

```
pip install -r requirements.txt -t ./
```

Build zip archive:

```
zip -r alexa-temperature.zip ./
```

Upload to Lambda function:

```
aws lambda update-function-code --function-name <ARN> --zip-file fileb://alexa-temperature.zip
```
